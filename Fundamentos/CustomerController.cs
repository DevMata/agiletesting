﻿namespace agiletesting.fundamentos
{
    public class CustomerController
    {
        public ActionResult ObtenerCliente(int id)
        {
            if (id == 0)
                return new NotFound();
            
            return new Ok();
        }        
    }
    
    public class ActionResult { }
    
    public class NotFound : ActionResult { }
 
    public class Ok : ActionResult { }
}