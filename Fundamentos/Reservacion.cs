﻿namespace agiletesting.fundamentos
{
    public class Reservacion
    {
        public Usuario HechaPor { get; set; }

        public bool PuedeCancelar(Usuario user)
        {
            return (user.EsAdmin || HechaPor == user);
        }        
    }

    public class Usuario
    {
        public bool EsAdmin { get; set; }
    }
}