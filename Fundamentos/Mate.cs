﻿using System.Collections.Generic;

namespace agiletesting.fundamentos
{
    public class Mate
    {
        public int Suma(int a, int b)
        { 
            return a + b;
        }
        
        public int Max(int a, int b)
        {
            return (a > b) ? a : b;
        }

        public IEnumerable<int> ObtenerNumImpares(int limit)
        {
            for (var i = 0; i <= limit; i++)
                if (i % 2 != 0)
                    yield return i; 
        }
    }
}