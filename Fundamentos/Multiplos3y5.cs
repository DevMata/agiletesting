﻿namespace agiletesting.fundamentos
{
    public class Multiplos3y5
    {
        public static string ObtenerSalida(int number)
        {
            if ((number % 3 == 0) && (number % 5 == 0))
                return "Múltiplo de ambos";

            if (number % 3 == 0)
                return "Múltiplo de 3";

            if (number % 5 == 0)
                return "Múltiplo de 5";

            return number.ToString(); 
        }
    }
}