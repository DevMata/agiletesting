﻿namespace agiletesting.mocking
{
    public class Producto
    {
        public float PrecioLista { get; set; }

        public float ObtenerPrecio(Cliente cliente)
        {
            if (cliente.EsVIP)
                return PrecioLista * 0.7f;

            return PrecioLista;
        }
    }

    public class Cliente
    {
        public bool EsVIP { get; set; }
    }
}