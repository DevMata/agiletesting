﻿using System.Net;

namespace agiletesting.Mocking
{
    public class Instalador
    {
        private string _destino;
        private IDownloader _downloader;

        public Instalador(IDownloader downloader)
        {
            _downloader = downloader;
        }

        public bool DescargarInstallador(string nombreCliente, string nombreInstalador)
        {           
            try
            {
                _downloader.DescargarArchivo(
                    string.Format("http://example.com/{0}/{1}",
                        nombreCliente,
                        nombreInstalador),
                    _destino);

                return true;
            }
            catch (WebException)
            {
                return false; 
            }
        }
    }
}