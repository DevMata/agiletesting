﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace agiletesting.Mocking
{
    public interface IDownloader
    {
        void DescargarArchivo(string url, string path);
    }

    public class Downloader : IDownloader
    {
        public void DescargarArchivo(string url, string path)
        {
            var cliente = new WebClient();

            cliente.DownloadFile(url, path);
        }
    }
}
