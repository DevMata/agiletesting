﻿namespace agiletesting.Solid.DependencyInversion
{
    class PostSolid 
    {
        private ILogger _logger;

        public PostSolid(ILogger logger)
        {
            _logger = logger;
        }

        void CrearPost(IBase bdd, string post)
        {
            try
            {
                bdd.Guardar(post);
            }catch(System.Exception ex)
            {
                _logger.log(ex.Message);
            }
        }
    }
}
