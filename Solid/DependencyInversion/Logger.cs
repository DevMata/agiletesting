﻿namespace agiletesting.Solid.DependencyInversion
{
    public interface ILogger
    {
        void log(string error);
    }

    public class Logger : ILogger
    {
        public void log(string error) { }
    }
}
