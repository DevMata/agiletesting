﻿namespace agiletesting.Solid.DependencyInversion
{
    class Post
    {
        private Logger errorLogger = new Logger();
        private Base bdd = new Base();

        void CrearPost(string post)
        {
            try
            {
                bdd.Guardar(post);
            }catch(System.Exception ex)
            {
                errorLogger.log(ex.Message);
            }
        }
    }
}
