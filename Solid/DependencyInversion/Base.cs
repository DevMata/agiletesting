﻿namespace agiletesting.Solid.DependencyInversion
{
    public interface IBase
    {
        void Guardar(string post);
    }

    public class Base : IBase
    {
        public void Guardar(string post) { }
    }
}
